<?php

/**
 * @file
 * Hooks related to AUO Api module.
 */

// phpcs:disable DrupalPractice.CodeAnalysis.VariableAnalysis.UnusedVariable

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the exportable webform element types.
 */
function hook_webform_adv_exp_not_allowed_element_types_alter(&$element_types) {
  // Exclude some custom elements form export.
  $element_types[] = 'unified_profile';
  $element_types[] = 'auo_webform_vertical_tab';
}

/**
 * Alter the generated export header columns.
 */
function hook_webform_adv_exp_destination_columns_alter(array &$columns, array $element_plugins, \Drupal\webform_adv_exp\Plugin\WebformHandler\AdvancedExportImport $handler) {
  foreach ($columns as $key => $column) {
    /** @var \Drupal\webform\Plugin\WebformElementInterface $element_plugin */
    if (isset($element_plugins[$key])) {
      $element_plugin = $element_plugins[$key];
      // The workflow component has a workflow_fieldset attribute which doesn't
      // have default value during the export. Let's remove it from columns.
      if ($element_plugin instanceof WebformWorkflowsElement) {
        $pieces = explode('__', $key);
        if ($pieces[1] == 'workflow_fieldset') {
          unset($columns[$key]);
        }
      }
    }
  }
}

/**
 * Alter the ignorable elements during an import.
 */
function hook_webform_adv_exp_not_importable_elements_alter(&$excluded) {
  // Exclude some custom elements form import.
  $excluded[] = 'first_name';
}

/**
 * @} End of "addtogroup hooks".
 */
