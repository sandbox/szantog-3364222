<?php

namespace Drupal\webform_adv_exp\Form;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\webform_submission_export_import\Form\WebformSubmissionExportImportUploadForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\ConfirmFormHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformMessage;
use Drupal\webform\Utility\WebformOptionsHelper;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Upload webform submission export import CSV.
 */
class UploadForm extends WebformSubmissionExportImportUploadForm {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The webform request handler.
   *
   * @var \Drupal\webform\WebformRequestInterface
   */
  protected $requestHandler;

  /**
   * The webform submission exporter.
   *
   * @var \Drupal\webform_adv_exp\Service\ExportImportManager
   */
  protected $importer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\webform_submission_export_import\Form\WebformSubmissionExportImportUploadForm $instance */
    $instance = parent::create($container);
    $instance->importer = $container->get('webform_adv_exp.manager');
    $instance->initialize();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_adv_exp_upload_form';
  }

  /* ************************************************************************ */
  // Upload form.
  /* ************************************************************************ */

  /**
   * Build upload form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   An associative array containing the structure of the form.
   */
  protected function buildUploadForm(array $form, FormStateInterface $form_state) {
    $webform = $this->importer->getWebform();
    // Warning.
    $form['experimental_warning'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_id' => 'webform_submission_export_import_experimental',
      '#message_close' => TRUE,
      '#message_storage' => WebformMessage::STORAGE_STATE,
      '#message_message' => $this->t('Importing submissions is a new and experimental feature.') . '<br/><strong>' . $this->t('Please test and review your imported submissions using a development/test server.') . '</strong>',
    ];

    // Details.
    $temporary_maximum_age = $this->config('system.file')->get('temporary_maximum_age');
    $form['details'] = [
      'title' => [
        '#markup' => $this->t('Please note'),
      ],
      'list' => [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('All submission properties and data is optional.'),
          $this->t('Be <strong>extremely careful</strong> updating entity references this way! Looking for the referenced entities by label might contain multiple results.'),
          $this->t('If UUIDs are included, existing submissions will always be updated.'),
          $this->t('If UUIDs are not included, already imported and unchanged records will not create duplication submissions.'),
          $this->t('File uploads must use publicly access URLs which begin with http:// or https://.'),
          $this->t('Composite (single) values are annotated using double underscores. (e.g. ELEMENT_KEY__SUB_ELEMENT_KEY) Example: status__workflow_state, where <em>status</em> is the machine name of the webform element, <em>workflow_state</em> is a part of the webform_workflows_element component.'),
          $this->t('Multiple values are comma delimited with any nested commas URI escaped (%2C).'),
          $this->t('Multiple composite values are formatted using <a href=":href">inline YAML</a>.', [':href' => 'https://en.wikipedia.org/wiki/YAML#Basic_components']),
          $this->t('Import maximum execution time limit is @time.', ['@time' => $this->dateFormatter->formatInterval($temporary_maximum_age)]),
        ],
      ],
    ];

    // Examples.
    $download_url = $this->requestHandler->getCurrentWebformUrl('webform_submission_export_import.results_import.example.download');
    $view_url = $this->requestHandler->getCurrentWebformUrl('webform_submission_export_import.results_import.example.view');
    $t_args = [
      ':href_download' => $download_url->toString(),
      ':href_view' => $view_url->toString(),
    ];
    $form['examples'] = [
      '#markup' => $this->t('<a href=":href_view">View</a> or <a href=":href_download">download</a> an example submission CSV.', $t_args),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    // Form.
    $form['import'] = [
      '#type' => 'details',
      '#title' => $this->t('Import data source'),
      '#open' => TRUE,
    ];
    $form['import']['import_type'] = [
      '#title' => 'Type',
      '#type' => 'radios',
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
      '#options' => [
        'file' => $this->t('File upload'),
        'url' => $this->t('Remote URL'),
      ],
      '#default_value' => 'file',
    ];
    $form['import']['import_file'] = [
      '#type' => 'file',
      '#title' => $this->t('Upload Submission CSV file'),
      '#states' => [
        'visible' => [
          ':input[name="import_type"]' => ['value' => 'file'],
        ],
        'required' => [
          ':input[name="import_type"]' => ['value' => 'file'],
        ],
      ],
    ];
    $form['import']['import_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Enter Submission CSV remote URL'),
      '#description' => $this->t('Remote URL could be a <a href=":href">published Google Sheet</a>.', [':href' => 'https://help.aftership.com/hc/en-us/articles/115008490908-CSV-Auto-Fetch-using-Google-Drive-Spreadsheet']),
      '#states' => [
        'visible' => [
          ':input[name="import_type"]' => ['value' => 'url'],
        ],
        'required' => [
          ':input[name="import_type"]' => ['value' => 'url'],
        ],
      ],
    ];

    $options = [];
    foreach ($webform->getHandlers() as $handler) {
      if ($handler->getStatus() && $handler->getPluginId() == 'webform_adv_exp') {
        $options[$handler->getHandlerId()] = $handler->getLabel();
      }
    }

    if (!empty($options)) {
      $form['import']['importer'] = [
        '#type' => 'select',
        '#title' => t('Select importer'),
        '#options' => $options,
        '#default_value' => count($options) == 1 ? array_key_first($options) : NULL,
        '#required' => TRUE,
      ];

      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Continue'),
        '#validate' => ['::validateUploadForm'],
        '#submit' => ['::submitUploadForm'],
      ];
    }
    else {
      $url = Url::fromRoute('entity.webform.handlers', ['webform' => $webform])->toString();
      $this->messenger()->addError($this->t('The webform does not contain any advanced exporter. Go to the <a href=":url">Webform handler</a> settings page and create one.', [':url' => $url]));
    }

    return $form;
  }

  /**
   * Upload submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitUploadForm(array &$form, FormStateInterface $form_state) {
    $validators = ['file_validate_extensions' => ['csv']];

    $import_type = $form_state->getValue('import_type');

    $file = NULL;
    switch ($import_type) {
      case 'file':
        $files = file_save_upload('import_file', $validators);
        $file = ($files) ? reset($files) : NULL;
        break;

      case 'url':
        $import_url = $form_state->getValue('import_url');
        $file_path = tempnam($this->fileSystem->getTempDirectory(), 'webform_submission_export_import_') . '.csv';
        $file_content = file_get_contents($import_url);
        if (!$file_content) {
          $this->messenger()->addError($this->t("Unable to parse CSV file from url: :url. Please review the CSV file's source.", [':url' => $import_url]));
          break;
        }
        file_put_contents($file_path, $file_content);

        $form_field_name = $this->t('Submission CSV (Comma Separated Values) file');
        $file_size = filesize($file_path);
        // Mimic Symfony and Drupal's upload file handling.
        $file_info = new UploadedFile($file_path, basename($file_path), NULL, $file_size);
        $file = _webform_submission_export_import_file_save_upload_single($file_info, $form_field_name, $validators);
        break;
    }

    // If a managed file has been created to the file's id and rebuild the form.
    if ($file) {
      $this->importer->setImportUri($file->getFileUri());
      if ($this->importer->getTotal() > 0) {
        $form_state->set('import_fid', $file->id());
        $form_state->set('importer', $form_state->getValue('importer'));
        $form_state->setRebuild();
      }
      else {
        $this->messenger()->addError($this->t("Unable to parse CSV file. Please review the CSV file's formatting."));
      }
    }
  }

  /* ************************************************************************ */
  // Confirm form.
  /* ************************************************************************ */

  /**
   * Build confirm import form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   An associative array containing the structure of the form.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildConfirmForm(array $form, FormStateInterface $form_state) {
    $webform = $this->importer->getWebform();
    $importer_plugin_id = $form_state->getValue('importer');
    $this->importer->setHandler($importer_plugin_id);
    $config = $webform->getHandler($importer_plugin_id)->getConfiguration();
    $import_options = $form_state->get('import_options') ?: [];
    $form['#disable_inline_form_errors'] = TRUE;
    $form['#attributes']['class'][] = 'confirmation';
    $form['#theme'] = 'confirm_form';
    $form[$this->getFormName()] = ['#type' => 'hidden', '#value' => 1];

    // Warning.
    $total = $this->importer->getTotal();
    $t_args = [
      '@submissions' => $this->formatPlural($total, '@count submission', '@count submissions'),
    ];
    $form['warning'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => $this->t('Are you sure you want to import @submissions?', $t_args) . '<br/>' .
      '<strong>' . $this->t('This action cannot be undone.') . '</strong>',
    ];

    // Details.
    $actions = [
      $this->t('Update submissions that have a corresponding UUID.'),
      $this->t('Create new submissions.'),
    ];
    if (!empty($import_options['skip_validation'])) {
      $actions[] = $this->t('Form validation will be skipped.');
    }
    else {
      $actions[] = $this->t('Skip submissions that are invalid.');
    }
    $form['details'] = [
      'title' => [
        '#markup' => $this->t('This action will…'),
      ],
      'list' => [
        '#theme' => 'item_list',
        '#items' => $actions,
      ],
    ];

    // Mapping.
    $source = $this->appendNameToOptions($this->importer->getSourceColumns());
    $destination = $this->appendNameToOptions($this->importer->getDestinationColumns($config['handler_id']));
    $mappings = $this->importer->getSourceToDestinationColumnMapping($config['handler_id']);
    $form['review'] = [
      '#type' => 'details',
      '#title' => $this->t('Review import'),
    ];
    // Displaying when no UUID, sid or token is found.
    if (!isset($source['uuid']) && !isset($source['sid']) && !isset($source['token'])) {
      $form['review']['warning'] = [
        '#type' => 'webform_message',
        '#message_type' => 'warning',
        '#message_message' => $this->t('No UUID, sid or token was found in the source (CSV). A unique hash will be generated for each CSV record. Any changes to already an imported record in the source (CSV) will create a new submission.', $t_args),
        '#message_close' => TRUE,
        '#message_storage' => WebformMessage::STORAGE_NONE,
      ];
    }
    $form['review']['mapping'] = [
      '#type' => 'webform_mapping',
      '#title' => $this->t('Import mapping'),
      '#source__title' => $this->t('Source (CSV)'),
      '#destination__title' => $this->t('Destination (Submission)'),
      '#description' => $this->t('Please review and select the imported CSV source column to destination element mapping'),
      '#description_display' => 'before',
      '#default_value' => $mappings,
      '#required' => TRUE,
      '#source' => $source,
      '#destination' => $destination,
      '#parents' => ['import_options', 'mapping'],
    ];

    // Options.
    $form['import_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Import options'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['import_options']['skip_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip form validation'),
      '#description' => $this->t('Skipping form validation can cause invalid data to be stored in the database.'),
      '#return_value' => TRUE,
    ];
    $form['import_options']['treat_warnings_as_errors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Treat all warnings as errors'),
      '#description' => $this->t("CSV data that can't be converted to submission data will display a warning. If checked, these warnings will be treated as errors and prevent the submission from being created."),
      '#return_value' => TRUE,
    ];
    $form['import_options']['update_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update only'),
      '#description' => $this->t("Disable creating new submissions, only existing ones will be updated."),
      '#default_value' => TRUE,
      '#return_value' => TRUE,
    ];
    $form['import_options']['autofix'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autofix'),
      '#description' => $this->t("Try to fix some whitespace error. Note! This can cause validation issues!"),
      '#return_value' => FALSE,
    ];

    // Confirm.
    $form['confirm'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Yes, I want to import these submissions'),
      '#required' => TRUE,
    ];

    // Actions.
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->getConfirmText(),
      '#button_type' => 'primary',
      '#submit' => ['::submitImportForm'],
    ];
    $form['actions']['cancel'] = ConfirmFormHelper::buildCancelLink($this, $this->getRequest());
    return $form;
  }

  /* ************************************************************************ */
  // Batch functions.
  // Using static method to prevent the service container from being serialized.
  // Prevents 'AssertionError: The container was serialized.' exception.
  /* ************************************************************************ */

  /**
   * Batch API; Initialize batch operations.
   *
   * @param \Drupal\webform\WebformInterface|null $webform
   *   A webform.
   * @param \Drupal\Core\Entity\EntityInterface|null $source_entity
   *   A webform source entity.
   * @param string $import_uri
   *   The URI of the CSV import file.
   * @param array $import_options
   *   An array of import options.
   *
   * @see http://www.jeffgeerling.com/blogs/jeff-geerling/using-batch-api-build-huge-csv
   */
  public static function batchSet(WebformInterface $webform, EntityInterface $source_entity = NULL, $import_uri = '', array $import_options = []) {
    $parameters = [
      $webform,
      $source_entity,
      $import_uri,
      $import_options,
    ];
    $batch = [
      'title' => t('Importing submissions'),
      'init_message' => t('Initializing submission import'),
      'error_message' => t('The import could not be completed because an error occurred.'),
      'operations' => [
        [
          ['\Drupal\webform_adv_exp\Form\UploadForm', 'batchProcess'],
          $parameters,
        ],
      ],
      'finished' => [
        '\Drupal\webform_submission_export_import\Form\WebformSubmissionExportImportUploadForm',
        'batchFinish',
      ],
    ];

    batch_set($batch);
  }

  /**
   * Batch API callback.
   *
   * Writes the header and rows of the export to the export file.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform.
   * @param \Drupal\Core\Entity\EntityInterface|null $source_entity
   *   A webform source entity.
   * @param string $import_uri
   *   The URI of the CSV import file.
   * @param array $import_options
   *   An associative array of import options.
   * @param mixed|array $context
   *   The batch current context.
   *
   * @throws \Exception
   */
  public static function batchProcess(WebformInterface $webform, EntityInterface $source_entity = NULL, $import_uri = '', array $import_options = [], &$context = []): void {
    /** @var \Drupal\webform_adv_exp\Service\ExportImportManager $importer */
    $importer = \Drupal::service('webform_adv_exp.manager');
    $importer->setWebform($webform);
    $importer->setSourceEntity($source_entity);
    $importer->setImportUri($import_uri);
    $importer->setImportOptions($import_options);

    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['offset'] = 0;
      $context['sandbox']['max'] = $importer->getTotal();
      // Drush is losing the results, so we are going to track theme
      // via the sandbox.
      $context['sandbox']['stats'] = [
        'created' => 0,
        'updated' => 0,
        'skipped' => 0,
        'corrupted' => -1,
        'total' => 0,
        'warnings' => [],
        'errors' => [],
      ];
      $context['results'] = [
        'import_uri' => $import_uri,
      ];
    }

    // Import CSV records.
    $import_stats = $importer->import($context['sandbox']['offset'], $importer->getBatchLimit());

    // Append import stats and errors to results.
    foreach ($import_stats as $import_stat => $value) {
      if (is_array($value)) {
        // Convert translatable markup into strings to save memory.
        $context['sandbox']['stats'][$import_stat] += WebformOptionsHelper::convertOptionsToString($value);
      }
      else {
        $context['sandbox']['stats'][$import_stat] += $value;
      }
    }

    // Track progress.
    $context['sandbox']['progress'] += $import_stats['total'];
    $context['sandbox']['offset'] += $importer->getBatchLimit();

    // Display message.
    $context['message'] = t('Imported @count of @total submissions…', [
      '@count' => $context['sandbox']['progress'],
      '@total' => $context['sandbox']['max'],
    ]);

    // Track finished.
    if ($context['sandbox']['progress'] !== $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }

    // Context results are not being passed to batchFinish via Drush,
    // therefor we are going to show them when this is finished.
    if ($context['finished'] >= 1) {
      static::displayStats($context['sandbox']['stats']);
    }
  }

  /**
   * Display import status.
   *
   * @param array $stats
   *   Import stats.
   */
  public static function displayStats(array $stats) {
    $is_cli = (PHP_SAPI === 'cli');
    $number_of_errors = 0;
    $error_limit = ($is_cli) ? NULL : 50;
    $t_args = [
      '@total' => $stats['total'],
      '@created' => $stats['created'],
      '@updated' => $stats['updated'],
      '@skipped' => $stats['skipped'],
      '@corrupted' => $stats['corrupted'],
    ];
    if ($is_cli) {
      \Drupal::logger('webform')
        ->notice(t('Submission import completed. (total: @total; created: @created; updated: @updated; skipped: @skipped)', $t_args));
    }
    else {
      \Drupal::messenger()
        ->addStatus(t('Submission import completed. (total: @total; created: @created; updated: @updated; skipped: @skipped.)', $t_args));
    }
    $message_types = [
      'warnings' => MessengerInterface::TYPE_WARNING,
      'errors' => MessengerInterface::TYPE_ERROR,
    ];
    foreach ($message_types as $message_group => $message_type) {
      foreach ($stats[$message_group] as $row_number => $messages) {
        $row_prefix = [
          '#markup' => t('Row #@number', ['@number' => $row_number]),
          '#prefix' => $is_cli ? '' : '<strong>',
          '#suffix' => $is_cli ? ': ' : ':</strong> ',
        ];
        foreach ($messages as $message) {
          if ($is_cli) {
            $message = strip_tags($message);
          }
          $build = [
            'row' => $row_prefix,
            'message' => ['#markup' => $message],
          ];
          $message = \Drupal::service('renderer')->renderPlain($build);
          if ($is_cli) {
            \Drupal::logger('webform_submission_export_import')
              ->$message_type($message);
          }
          else {
            \Drupal::messenger()->addMessage($message, $message_type);
          }
          if ($error_limit && ++$number_of_errors >= $error_limit) {
            return;
          }
        }
      }
    }
  }

}
