<?php

namespace Drupal\webform_adv_exp;

/**
 * Trait of advanced export.
 */
trait AdvancedExportTrait {

  /**
   * Webform submission base field to be never exported.
   */
  public function getNeverExportedFields() {
    return [
      'token',
      'webform_id',
      'entity_type',
      'entity_id',
      'uri',
    ];
  }

}
