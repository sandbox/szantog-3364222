<?php

namespace Drupal\webform_adv_exp\Service;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\Plugin\WebformElement\WebformLikert;
use Drupal\webform\Plugin\WebformElement\WebformManagedFileBase;
use Drupal\webform\Plugin\WebformElementEntityReferenceInterface;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_submission_export_import\WebformSubmissionExportImportImporter;
use Drupal\webform_workflows_element\Plugin\WebformElement\WebformWorkflowsElement;
use Drupal\webform_workflows_element\Service\WebformWorkflowsManager;
use Symfony\Component\Yaml\Dumper;

/**
 * Webform submission export importer.
 */
class ExportImportManager extends WebformSubmissionExportImportImporter {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * A webform_adv_exp instance.
   *
   * @var \Drupal\webform_adv_exp\Plugin\WebformHandler\AdvancedExportImport
   */
  protected $handler;

  /**
   * The webform_workflows_element.manager service.
   *
   * @var \Drupal\webform_workflows_element\Service\WebformWorkflowsManager
   */
  protected $webformWorkflowsManager;

  /**
   * The logger.channel.webform_adv_exp service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a WebformSubmissionExportImport object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              LoggerChannelFactoryInterface $logger_factory,
                              EntityTypeManagerInterface $entity_type_manager,
                              WebformElementManagerInterface $element_manager,
                              FileSystemInterface $file_system,
                              RequestStack $requestStack,
                              ModuleHandlerInterface $moduleHandler,
                              WebformWorkflowsManager $workflowsManager) {
    parent::__construct($config_factory, $logger_factory, $entity_type_manager, $element_manager, $file_system);
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->moduleHandler = $moduleHandler;
    $this->webformWorkflowsManager = $workflowsManager;
    $this->logger = $this->loggerFactory->get('webform_adv_exp');
  }

  /**
   * Sets the handler to work with.
   *
   * This service supports a specific handler added to a webform.
   */
  public function setHandler(string $plugin_id = NULL) {
    if (!empty($this->currentRequest->query->get('handler'))) {
      $plugin_id = $this->currentRequest->query->get('handler');
    }
    if (!empty($this->getWebform()->getHandler($plugin_id))) {
      $this->handler = $this->getWebform()->getHandler($plugin_id);
    };
  }

  /**
   * Gets the webform handler.
   */
  public function getHandler() {
    return $this->handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultImportOptions() {
    return [
      'skip_validation' => FALSE,
      'treat_warnings_as_errors' => FALSE,
      'mapping' => [],
      'update_only' => TRUE,
      'autofix' => TRUE,
    ];
  }

  /* ************************************************************************ */
  // Export.
  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exportHeader() {
    if (empty($this->handler)) {
      $this->setHandler();
    }
    return array_keys($this->getDestinationColumns($this->handler->getPluginId()));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function exportSubmission(WebformSubmissionInterface $webform_submission, array $export_options = []) {
    $submission_data = $webform_submission->toArray(TRUE);
    $handler = $this->webform->getHandler($export_options['handler']);
    $handler_config = $handler->getConfiguration();
    $allowed_fields = $handler_config['settings']['allowed_fields'];

    $record = [];

    // Append fields.
    $field_definitions = $this->getFieldDefinitions();
    foreach ($field_definitions as $field_name => $field_definition) {
      if (isset($allowed_fields[$field_name]) && $allowed_fields[$field_name] == TRUE) {
        switch ($field_name) {
          case 'uid':
            $value = $this->getEntityExportId($webform_submission->getOwner(), $export_options);
            break;

          case 'entity_id':
            $value = $this->getEntityExportId($webform_submission->getSourceEntity(), $export_options);
            break;

          default:
            $value = $submission_data[$field_name] ?? '';
            break;
        }
        $record[] = $this->exportValue($value);
      }
    }

    // Append elements.
    $elements = $this->getElements();
    foreach ($elements as $field_name => $element) {
      if (isset($allowed_fields[$field_name]) && $allowed_fields[$field_name] == TRUE || !empty($allowed_fields[$field_name])) {
        $element_plugin = $this->elementManager->getElementInstance($element);
        $has_multiple_values = $element_plugin->hasMultipleValues($element);
        if ($element_plugin instanceof WebformManagedFileBase) {
          // Files: Get File URLS.
          /** @var \Drupal\file\FileInterface $files */
          $files = $element_plugin->getTargetEntities($element, $webform_submission) ?: [];
          $values = [];
          foreach ($files as $file) {
            $values[] = $file->createFileUrl(FALSE);
          }
          $value = implode(',', $values);
          $record[] = $this->exportValue($value);
        }
        elseif ($element_plugin instanceof WebformElementEntityReferenceInterface) {
          // Entity references: Get entity UUIDs.
          $entities = $element_plugin->getTargetEntities($element, $webform_submission);
          $values = [];
          /** @var \Drupal\Core\Entity\EntityInterface $entity */
          foreach ($entities as $entity) {
            // The original behavior.
            // There's an option to export ID or UUID, but we need labels.
            // $values[] = $this->getEntityExportId($entity, $export_options).
            $values[] = $entity->label();
          }
          $value = implode(',', $values);
          $record[] = $this->exportValue($value);
        }
        elseif ($element_plugin instanceof WebformLikert) {
          // Single Composite: Split questions into individual columns.
          $value = $element_plugin->getValue($element, $webform_submission);
          $question_keys = array_keys($element['#questions']);
          foreach ($question_keys as $question_key) {
            $question_value = $value[$question_key] ?? '';
            $record[] = $this->exportValue($question_value);
          }
        }
        elseif ($element_plugin instanceof WebformCompositeBase && !$has_multiple_values) {
          // Composite: Split single composite sub elements into individual
          // columns.
          $value = $element_plugin->getValue($element, $webform_submission);
          $composite_element_keys = array_keys($element_plugin->getCompositeElements());
          foreach ($composite_element_keys as $composite_element_key) {
            if (isset($allowed_fields[$field_name][$composite_element_key]) && $allowed_fields[$field_name][$composite_element_key] == TRUE) {
              $composite_value = $value[$composite_element_key] ?? '';
              $record[] = $this->exportValue($composite_value);
            }
          }
        }
        elseif ($element_plugin->isComposite()) {
          // Composite: Convert multiple composite values to a single line of
          // YAML.
          $value = $element_plugin->getValue($element, $webform_submission);
          $dumper = new Dumper(2);
          $record[] = $dumper->dump($value);
        }
        elseif ($has_multiple_values) {
          // Multiple: Convert to comma separated values with commas URL
          // encodes.
          $values = $element_plugin->getValue($element, $webform_submission);
          $values = ($values !== NULL) ? (array) $values : [];
          foreach ($values as $index => $value) {
            $values[$index] = str_replace(',', '%2C', $value);
          }
          $value = implode(',', $values);
          $record[] = $this->exportValue($value);
        }
        else {
          // Default: Convert NULL values to empty strings.
          $value = $element_plugin->getValue($element, $webform_submission);
          $value = ($value !== NULL) ? $value : '';
          $record[] = $this->exportValue($value);
        }
      }
    }

    return $record;
  }

  /* ************************************************************************ */
  // Import.
  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function import($offset = 0, $limit = NULL) {
    if ($limit === NULL) {
      $limit = $this->getBatchLimit();
    }

    $import_options = $this->getImportOptions();

    // Open CSV file.
    $handle = fopen($this->getImportUri(), 'r');

    // Get the column names.
    $column_names = fgetcsv($handle);
    foreach ($column_names as $index => $name) {
      $column_names[$index] = $name;
    }

    // Fast-forward CSV file to offset.
    $index = 0;
    while ($index < $offset && !feof($handle)) {
      fgetcsv($handle);
      $index++;
    }

    // Collect import stats.
    $stats = [
      'created' => 0,
      'updated' => 0,
      'skipped' => 0,
      'corrupted' => 0,
      'total' => 0,
      'warnings' => [],
      'errors' => [],
    ];

    // Import submission records.
    while ($stats['total'] < $limit && !feof($handle)) {
      // Get CSV values.
      $values = fgetcsv($handle);
      // Complete ignored empty rows.
      if (empty($values) || $values === ['']) {
        continue;
      }
      $index++;
      $stats['total']++;

      // Track row specific warnings and errors.
      $stats['warnings'][$index] = [];
      $stats['errors'][$index] = [];
      $row_warnings = &$stats['warnings'][$index];
      $row_errors = &$stats['errors'][$index];

      // Make sure expected number of columns and values are equal.
      if (count($column_names) !== count($values)) {
        $t_args = [
          '@expected' => count($column_names),
          '@found' => count($values),
        ];
        $error = $this->t('@expected values expected and only @found found.', $t_args);
        $stats['corrupted']++;

        if (!empty($import_options['treat_warnings_as_errors'])) {
          $row_errors[] = $error;
        }
        else {
          $row_warnings[] = $error;
        }

        $error = $error . ' ' . serialize($values);
        $this->logger->info($error, $t_args);
        continue;
      }

      // Create record and trim all values.
      $record = array_combine($column_names, $values);
      foreach ($record as $key => $value) {
        $record[$key] = trim($value);
      }

      // Track original record.
      $original_record = $record;

      // Map.
      $record = $this->importMapRecord($record);

      // Token: Generate token from the original CSV record.
      if (empty($record['token'])) {
        $record['token'] = Crypt::hashBase64(Settings::getHashSalt() . serialize($original_record));
      }

      // Prepare.
      $webform_submission = $this->importLoadSubmission($record);
      if ($errors = $this->importPrepareRecord($record, $webform_submission)) {
        if (!empty($import_options['treat_warnings_as_errors'])) {
          $row_errors = array_merge($row_warnings, array_values($errors));
        }
        else {
          $row_warnings = array_merge($row_warnings, array_values($errors));
        }
      }

      // Validate.
      if (empty($import_options['skip_validation'])) {
        if ($errors = $this->importValidateRecord($record)) {
          $row_errors = array_merge($row_errors, array_values($errors));
        }
      }

      // Skip import if there are row errors.
      if ($row_errors) {
        $stats['skipped']++;
        continue;
      }

      // Save.
      if ($import_options['update_only']) {
        if (!is_null($webform_submission)) {
          // Only increase the updated stat when a real update happens.
          try {
            if ($this->importSaveSubmission($record, $webform_submission)) {
              $stats['updated']++;
            }
            else {
              $stats['skipped']++;
            }
          }
          catch (EntityStorageException $e) {
            // @todo Better logging.
            $stats['skipped']++;
          }
        }
        else {
          $stats['skipped']++;
        }
      }
      else {
        $this->importSaveSubmission($record);
        $stats['created']++;
      }
    }

    fclose($handle);
    return $stats;
  }

  /**
   * Save import record submission.
   *
   * @param array $record
   *   The record to be imported.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The existing webform submission.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function importSaveSubmission(array $record, WebformSubmissionInterface $webform_submission = NULL) {
    $field_definitions = $this->getFieldDefinitions();
    $elements = $this->getElements();
    $import_options = $this->getImportOptions();
    $changed = FALSE;
    $excluded = [
      'webform_id',
      'token',
      'entity_type',
      'entity_id',
    ];
    $this->moduleHandler->alter('webform_adv_exp_not_importable_elements', $excluded, $record, $webform_submission);
    if ($webform_submission) {
      // Update submission.
      unset($record['sid'], $record['serial'], $record['uuid']);
      foreach ($record as $name => $value) {
        // Let's try to fix some possible whitespace issue.
        if (isset($import_options['autofix']) && $import_options['autofix'] == TRUE) {
          if (is_array($value)) {
            foreach ($value as $key => $item) {
              $value[$key] = trim($item);
            }
          }
          else {
            $value = trim($value);
            // @todo Some \r\n is changed to \n.
          }
        }
        if (in_array($name, $excluded)) {
          continue;
        }
        if (isset($field_definitions[$name])) {
          $original_data = $webform_submission->get($name)->value;
          if (!empty($value) && $original_data !== $value) {
            $webform_submission->set($name, $value);
            $changed = TRUE;
          }

        }
        elseif (isset($elements[$name])) {
          $original_data = $webform_submission->getElementData($name);
          // If (is_array($value)) {
          // $g = array_diff($original_data, $value);
          // }.
          if (!empty($value) && $original_data !== $value) {
            $webform_submission->setElementData($name, $value);
            $changed = TRUE;
          }
        }
      }
    }
    else {
      // Create submission.
      $changed = TRUE;
      unset($record['sid'], $record['serial']);
      $values = $this->importConvertRecordToValues($record);
      $webform_submission = $this->getSubmissionStorage()->create($values);
    }
    if ($changed) {
      $webform_submission->save();
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Map source (CSV) record to destination (submission) records.
   *
   * @param array $record
   *   The source (CSV) record.
   *
   * @return array
   *   The destination (submission) records.
   */
  protected function importMapRecord(array $record) {
    $mapping = $this->getImportOption('mapping');

    // If not mapping is defined return the record AS-IS.
    if (empty($mapping)) {
      return $record;
    }

    $mapped_record = [];
    foreach ($mapping as $source_name => $destination_name) {
      if (isset($record[$source_name])) {
        $mapped_record[$destination_name] = $record[$source_name];
      }
    }
    return $mapped_record;
  }

  /**
   * Load import submission record via UUID or token.
   *
   * @param array $record
   *   The import submission record.
   *
   * @return \Drupal\webform\WebformSubmissionInterface|null
   *   The existing webform submission or NULL if no existing submission found.
   */
  protected function importLoadSubmission(array &$record) {
    $unique_keys = ['uuid', 'token', 'sid'];
    foreach ($unique_keys as $unique_key) {
      if (!empty($record[$unique_key])) {
        if ($webform_submissions = $this->getSubmissionStorage()->loadByProperties([$unique_key => $record[$unique_key]])) {
          return reset($webform_submissions);
        }
      }
    }
    return NULL;
  }

  /**
   * Prepare import submission record.
   *
   * @param array $record
   *   The import submission record.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The existing webform submission.
   *
   * @return array
   *   An array of error messages.
   *
   * @throws \Exception
   */
  protected function importPrepareRecord(array &$record, WebformSubmissionInterface $webform_submission = NULL) {
    // Track errors.
    $errors = [];
    $submission_values = [];
    if ($webform_submission) {
      $submission_values = $webform_submission->getData();
    }

    if (isset($record['uid'])) {
      // Convert user id to internal IDs.
      $record['uid'] = $this->getEntityImportId('user', $record['uid']);
      // Convert empty uid to anonymous user (UID: 0).
      if (empty($record['uid'])) {
        $record['uid'] = 0;
      }
    }

    // Remove empty uuid.
    if (empty($record['uuid'])) {
      unset($record['uuid']);
    }

    $webform = $this->getWebform();
    $source_entity = $this->getSourceEntity();

    // Set webform id.
    $record['webform_id'] = $webform->id();

    // Set source entity.
    // Load or convert the source entity id to an internal ID.
    if ($source_entity && !isset($record['entity_type']) && !isset($record['entity_id'])) {
      $record['entity_type'] = $source_entity->getEntityTypeId();
      $record['entity_id'] = $source_entity->id();
    }
    elseif (!empty($record['entity_type']) && isset($record['entity_id'])) {
      $record['entity_id'] = $this->getEntityImportId($record['entity_type'], $record['entity_id']);
      // If source entity_id can't be found, log error, and
      // remove the source  entity_type.
      if ($record['entity_id'] === NULL) {
        $t_args = [
          '@entity_type' => $record['entity_type'],
          '@entity_id' => $record['entity_id'],
        ];
        $errors[] = $this->t('Unable to locate source entity (@entity_type:@entity_id)', $t_args);
        $record['entity_type'] = NULL;
      }
    }

    // Convert record to submission element data.
    $elements = $this->getElements();
    foreach ($record as $name => $value) {
      // Set record value form an element.
      if (isset($elements[$name])) {
        $element = $elements[$name];
        $record[$name] = $this->importElement($element, $value, $webform_submission, $errors);
        continue;
      }

      // Check if record name is a composite element which is
      // delimited using '__'.
      if (strpos($name, '__') === FALSE) {
        continue;
      }

      // Get element and composite key and confirm that the element exists.
      [$element_key, $composite_key] = explode('__', $name);
      if (!isset($elements[$element_key])) {
        continue;
      }

      // Make sure the composite element is not storing multiple values which
      // must use YAML.
      // @see \Drupal\webform_adv_exp\WebformSubmissionExportImportImporter::importCompositeElement
      $element = $elements[$element_key];
      $element_plugin = $this->elementManager->getElementInstance($element);
      if ($element_plugin->hasMultipleValues($element)) {
        continue;
      }

      if ($element_plugin instanceof WebformLikert) {
        // Make sure the Likert question exists.
        if (!isset($element['#questions']) || !isset($element['#questions'][$composite_key])) {
          continue;
        }

        $record[$element_key][$composite_key] = $value;
      }
      elseif ($element_plugin instanceof WebformCompositeBase) {
        if ($element_plugin instanceof WebformWorkflowsElement) {
          $result = $this->importWorkflowsElement($element, $value, $webform_submission, $errors, $element_plugin, $composite_key);
          if (!empty($result)) {
            $record[$element_key] = $result;
          }
          else {
            continue;
          }
        }

        // Get the composite element and make sure it exists.
        // @todo Composite elements might have unfilled subelements. Need to deal with it.
        $composite_elements = $element_plugin->getCompositeElements();
        if (!isset($composite_elements[$composite_key])) {
          continue;
        }

        $composite_element = $composite_elements[$composite_key];
        $record[$element_key][$composite_key] = $this->importElement($composite_element, $value, $webform_submission, $errors);
      }
    }

    // FIll up record in order to prevent validation errors when updating.
    foreach ($submission_values as $key => $value) {
      // If key does doesn't exist, just add it.
      if (!isset($record[$key])) {
        $record[$key] = $value;
      }

    }
    return $errors;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function getDestinationColumns() {
    $handler = $this->getHandler();
    if (empty($handler)) {
      throw new PluginNotFoundException('of exporter in ' . $this->getWebform()->id() . ' webform');
    }

    $handler_config = $handler->getConfiguration();
    $allowed_fields = $handler_config['settings']['allowed_fields'];

    $columns = [];
    $element_plugins = [];

    $field_definitions = $this->getFieldDefinitions();
    foreach ($field_definitions as $field_name => $field_definition) {
      if (isset($allowed_fields[$field_name]) && $allowed_fields[$field_name] == TRUE) {
        $columns[$field_name] = $field_definition['title'];
      }
    }

    $elements = $this->getElements();
    foreach ($elements as $element_key => $element) {
      if (isset($allowed_fields[$element_key]) && ($allowed_fields[$element_key] == TRUE || !empty($allowed_fields[$element_key]))) {
        $element_plugin = $this->elementManager->getElementInstance($element);
        $element_title = $element_plugin->getAdminLabel($element);
        $has_multiple_values = $element_plugin->hasMultipleValues($element);
        if (!$has_multiple_values && $element_plugin instanceof WebformCompositeBase) {
          $composite_elements = $element_plugin->getCompositeElements();
          foreach ($composite_elements as $composite_element_key => $composite_element) {
            if (isset($allowed_fields[$element_key][$composite_element_key]) && $allowed_fields[$element_key][$composite_element_key] == TRUE) {
              $composite_element_name = $element_key . '__' . $composite_element_key;
              $composite_element_plugin = $this->elementManager->getElementInstance($composite_element);
              $composite_element_title = $composite_element_plugin->getAdminLabel($composite_element);
              $t_args = [
                '@element_title' => $element_title,
                '@composite_title' => $composite_element_title,
              ];
              $columns[$composite_element_name] = $this->t('@element_title: @composite_title', $t_args);
              $element_plugins[$composite_element_name] = $element_plugin;
            }
          }
        }
        elseif (!$has_multiple_values && $element_plugin instanceof WebformLikert) {
          $questions = $element['#questions'];
          foreach ($questions as $question_key => $question) {
            $question_element_name = $element_key . '__' . $question_key;
            $t_args = [
              '@element_title' => $element_title,
              '@question_title' => $question,
            ];
            $columns[$question_element_name] = $this->t('@element_title: @question_title', $t_args);
            $element_plugins[$question_element_name] = $element_plugin;
          }
        }
        else {
          $columns[$element_key] = $element_title;
          $element_plugins[$element_key] = $element_plugin;
        }
      }
    }

    $this->moduleHandler->alter('webform_adv_exp_destination_columns', $columns, $element_plugins, $this->handler);

    return $columns;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function importEntityReferenceElement(array $element, $value, WebformSubmissionInterface $webform_submission = NULL, array &$errors) {
    $element_plugin = $this->elementManager->getElementInstance($element);
    $entity_type_id = $element_plugin->getTargetType($element);
    $entity_storage = $this->getEntityStorage($entity_type_id);
    $values = explode(',', $value);
    foreach ($values as $index => $value) {
      $property = [
        'name',
        'title',
      ];
      $entities = $entity_storage->loadByProperties([$property => $value]);
      if ($entities) {
        if (count($entities > 1)) {
          $t_args = [
            '@element_key' => $element['#webform_key'],
            '@entity_type' => $entity_type_id,
            '@label' => $value,
          ];
          $errors[] = $this->t('[@element_key] Unable to locate @entity_type entity by label (@label), because there are more entity with the same label.', $t_args);
          unset($values[$index]);
        }
        elseif (count($entities) == 1) {
          $t_args = [
            '@element_key' => $element['#webform_key'],
            '@entity_type' => $entity_type_id,
            '@label' => $value,
          ];
          $errors[] = $this->t('[@element_key] Unable to locate @entity_type entity by label (@label), because there is no entity with this label.', $t_args);
          unset($values[$index]);
        }
        else {
          $entity = reset($entities);
          $values[$index] = $entity->id();
        }
      }
    }
    $values = array_values($values);
    if (empty($values)) {
      return ($element_plugin->hasMultipleValues($element)) ? [] : NULL;
    }
    else {
      return ($element_plugin->hasMultipleValues($element)) ? $values : reset($values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceToDestinationColumnMapping() {
    // @todo This might need more tweaks. We might want to map every value of submission despite of added it as allowed fields.
    $mapping = parent::getSourceToDestinationColumnMapping();
    // $source_column_names = $this->getSourceColumns();
    // $destination_column_names = $this->getDestinationColumns();
    return $mapping;
  }

  /**
   * Imports a workflow element.
   *
   * Workflow element is a complex composite field. It must be updated by the
   * submission state label or machine name, and we have to check if a
   * transition is allowed between the old and new values.
   *
   * @throws \Exception
   */
  protected function importWorkflowsElement(array $element, $value, WebformSubmissionInterface $webform_submission = NULL, array &$errors, WebformWorkflowsElement $element_plugin, string $composite_key) {
    // @todo This should probably the initial workflow state. We don't create submissions yet, this is the ticket when needed: AUO-1946
    $current_value = [];

    if ($webform_submission) {
      $current_value = $webform_submission->getElementData($element['#webform_key']);
    }

    // We operate with workflow_state or workflow_label only.
    if (!($composite_key == 'workflow_state' || $composite_key == 'workflow_label')) {
      return $current_value;
    }

    /** @var \Drupal\webform_workflows_element\Plugin\WebformElement\WebformWorkflowsElement $workflow */
    $workflow = $this->webformWorkflowsManager->getWorkflowTypeFromElement($element);
    $states = $this->webformWorkflowsManager->getStatesFromElement($element);

    // Gets new state based on workflow state or label.
    if ($composite_key == 'workflow_state') {
      $new_state = $this->webformWorkflowsManager->getStateFromElementAndId($element, $value);
    }
    else {
      /** @var \Drupal\workflows\State $state */
      foreach ($states as $state) {
        if ($state->label() == $value) {
          $new_state = $this->webformWorkflowsManager->getStateFromElementAndId($element, $state->id());
        }
      }
    }

    if (!$new_state) {
      $t_args = [
        '@element_key' => $element['#webform_key'],
        '@value' => $value,
        '@workflow' => $workflow->label(),
      ];
      $errors[] = $this->t('[@element_key] Unable to get @value state of @workflow workflow.', $t_args);
      return $current_value;
    }

    if (isset($current_value['workflow_state'])) {
      $current_state = $this->webformWorkflowsManager->getStateFromElementAndId($element, $current_value['workflow_state']);
    }
    else {
      return $current_value;
    }

    if ($current_state->id() == $new_state->id()) {
      return $current_value;
    }

    $transitions = $this->webformWorkflowsManager->getValidTransitions($workflow, $current_state);

    /** @var \Drupal\workflows\Transition $transition */
    foreach ($transitions as $transition) {
      $to = $transition->to();
      // @todo We are not affected yet, but theoretically there might be use case when the new state is a target of multiple transitions. We should deal with it, maybe extend the composite element somehow or at least log error.
      if ($new_state->id() == $to->id()) {
        return [
          'workflow_state' => $new_state->id(),
          'workflow_state_previous' => $current_state->id(),
          'workflow_state_label' => $new_state->label(),
          'transition' => $transition->id(),
          'changed_timestamp' => strtotime('now'),
          'changed_user' => \Drupal::currentUser()->id(),
        ];
      }
    }

    // If we reach this point, we can be sure that we can't switch state.
    $t_args = [
      '@element_key' => $element['#webform_key'],
      '@current_state' => $current_state->label(),
      '@new_state' => $new_state->label(),
    ];
    $errors[] = $this->t('[@element_key] Transition is not allowed between @current_state and @new_state.', $t_args);

    return $current_value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotal() {
    if (isset($this->importTotal)) {
      return $this->importTotal;
    }
    // Ignore the header.
    $total = -1;
    $handle = fopen($this->importUri, 'r');
    while (!feof($handle)) {
      $line = fgetcsv($handle);
      if (!empty($line)) {
        $total++;
      }
    }
    $this->importTotal = $total;
    return $this->importTotal;
  }

}
