<?php

namespace Drupal\webform_adv_exp\Plugin\WebformExporter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformExporter\FileHandleTraitWebformExporter;
use Drupal\webform\Plugin\WebformExporterBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an advanced CSV export.
 *
 * @WebformExporter(
 *   id = "webform_adv_exp_exporter",
 *   label = @Translation("Advanced exports"),
 *   description = @Translation("Exports results in CSV that can be imported
 *   back into the current webform."),
 *   archive = FALSE,
 *   files = FALSE,
 *   options = FALSE,
 * )
 */
class AdvancedExporter extends WebformExporterBase {

  use FileHandleTraitWebformExporter;

  /**
   * The webform_adv_exp.manager service..
   *
   * @var \Drupal\webform_adv_exp\Service\ExportImportManager
   */
  protected $exportImportManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->exportImportManager = $container->get('webform_adv_exp.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() +
      [
        'handler' => '',
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $build_info = $form_state->getBuildInfo();
    // The common exporters settings page does not have webform.
    $webform = FALSE;
    if (isset($build_info['args'][0])) {
      /** @var \Drupal\webform\Entity\Webform $webform */
      $webform = $build_info['args'][0];
      $config = $this->getConfiguration();
      $config['webform'] = $webform;
      $this->setConfiguration($config);
    }

    $form['warning'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => $this->t('<strong>Warning:</strong> Opening delimited text files with spreadsheet applications may expose you to <a href=":href">formula injection</a> or other security vulnerabilities. When the submissions contain data from untrusted users and the downloaded file will be used with Microsoft Excel, use \'HTML table\' format.', [':href' => 'https://www.google.com/search?q=spreadsheet+formula+injection']),
    ];

    if ($webform) {
      $form['handler'] = [
        '#type' => 'select',
        '#title' => $this->t('Choose an exporter'),
        '#options' => $this->getHandlers(),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileExtension() {
    return 'csv';
  }

  /**
   * {@inheritdoc}
   */
  public function writeHeader() {
    $config = $this->getConfiguration();
    $header = $this->getExportImportManager()->exportHeader($config['handler']);
    fputcsv($this->fileHandle, $header);
  }

  /**
   * {@inheritdoc}
   */
  public function writeSubmission(WebformSubmissionInterface $webform_submission) {
    $record = $this->getExportImportManager()->exportSubmission($webform_submission, $this->configuration);
    fputcsv($this->fileHandle, $record);
  }

  /**
   * Get the submission importer.
   */
  protected function getExportImportManager() {
    $this->exportImportManager->setWebform($this->getWebform());
    return $this->exportImportManager;
  }

  /**
   * Gets the attached advanced export handlers.
   */
  private function getHandlers() {
    $options = [];
    foreach ($this->getWebform()->getHandlers() as $handler) {
      if ($handler->getStatus() && $handler->getPluginId() == 'webform_adv_exp') {
        $options[$handler->getHandlerId()] = $handler->getLabel();
      }
    }

    return $options;
  }

}
