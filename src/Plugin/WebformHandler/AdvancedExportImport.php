<?php

namespace Drupal\webform_adv_exp\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\Plugin\WebformElement\WebformLikert;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform_adv_exp\AdvancedExportTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity reference access check handler..
 *
 * @WebformHandler(
 *   id = "webform_adv_exp",
 *   label = @Translation("Advanced export"),
 *   category = @Translation("Import/export"),
 *   description = @Translation("You can add configurable export options."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class AdvancedExportImport extends WebformHandlerBase {

  use AdvancedExportTrait;

  /**
   * The webform_adv_exp.manager service.
   *
   * @var \Drupal\webform_adv_exp\Service\ExportImportManager
   */
  protected $exportImportManager;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The plugin.manager.webform.element service.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * Defines element types which are not allowed to export.
   *
   * @var array
   */
  private $notAllowedElementTypes;

  /**
   * {@inheritdoc}
   *
   * IMPORTANT:
   * Webform handlers are initialized and serialized when they are attached to a
   * webform. Make sure not include any services as a dependency injection
   * that directly connect to the database. This will prevent
   * "LogicException: The database connection is not serializable." exceptions
   * from being thrown when a form is serialized via an Ajax callback and/or
   * form build.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->exportImportManager = $container->get('webform_adv_exp.manager');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->elementManager = $container->get('plugin.manager.webform.element');
    $instance->notAllowedElementTypes = self::getNotAllowedElementTypes();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $settings = $this->getSettings();
    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'allowed_fields' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $fieldDefinitions = $this->exportImportManager->getFieldDefinitions();
    $form['allowed_fields'] = [];

    foreach ($fieldDefinitions as $fieldDefinition) {
      if (!in_array($fieldDefinition['name'], $this->getNeverExportedFields())) {
        $form['allowed_fields'] += $this->getFieldDefinitionConfigFormElement($fieldDefinition);
      }
    }

    foreach ($this->getWebform()->getElementsDecoded() as $name => $element) {
      if (is_null($this->getElementConfigFormElement($name, $element))) {
        $form['allowed_fields'] += $this->getChildElementsConfigFormElements($element);
      }
      else {
        $form['allowed_fields'] += $this->getElementConfigFormElement($name, $element);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * Gets a field definition config form element.
   */
  protected function getFieldDefinitionConfigFormElement($fieldDefinition) {
    $allowedFields = $this->configuration['allowed_fields'];
    $fieldName = $fieldDefinition['name'];
    if (in_array($fieldName, $this->notAllowedElementTypes)) {
      return;
    }

    $element = [
      $fieldName => [
        '#type' => 'checkbox',
        '#title' => $fieldDefinition['title'],
      ],
    ];

    if (isset($allowedFields[$fieldName])) {
      $element[$fieldName]['#default_value'] = $allowedFields[$fieldName];
    }

    return $element;
  }

  /**
   * Gets an array of config form of an element.
   *
   * @throws \Exception
   */
  protected function getElementConfigFormElement(string $fieldName, array $element) {
    $return = [];
    $notAllowedElements = $this->getNotAllowedElementTypes();
    $allowedFields = $this->configuration['allowed_fields'];
    if (in_array($element['#type'], $notAllowedElements)) {
      return;
    }

    if (empty($element['#title'])) {
      return;
    }

    $element_plugin = $this->elementManager->getElementInstance($element);

    // Composite elements has subfields that might want to be included to an
    // export one by one.
    if ($element_plugin instanceof WebformCompositeBase) {
      $return[$fieldName] = [
        '#type' => 'fieldset',
        '#title' => $element['#title'],
      ];

      $composite_elements = $element_plugin->getCompositeElements();

      foreach ($composite_elements as $composite_element_key => $composite_element) {
        $composite_element_plugin = $this->elementManager->getElementInstance($composite_element);
        $composite_element_title = $composite_element_plugin->getAdminLabel($composite_element);

        $return[$fieldName][$composite_element_key] = [
          '#type' => 'checkbox',
          '#title' => $composite_element_title,
        ];

        if (isset($allowedFields[$fieldName][$composite_element_key])) {
          $return[$fieldName][$composite_element_key]['#default_value'] = $allowedFields[$fieldName][$composite_element_key];
        }
      }
    }
    elseif ($element_plugin instanceof WebformLikert) {
      // @todo Need some speciality?
    }
    else {
      $return = [
        $fieldName => [
          '#type' => 'checkbox',
          '#title' => $element['#title'],
        ],
      ];

      if (isset($allowedFields[$fieldName])) {
        $return[$fieldName]['#default_value'] = $allowedFields[$fieldName];
      }
    }

    return $return;
  }

  /**
   * Gets an array of config form of an element's children.
   *
   * @throws \Exception
   */
  protected function getChildElementsConfigFormElements($element) {
    $return = [];
    foreach (Element::children($element) as $child) {
      if (is_null($this->getElementConfigFormElement($child, $element[$child]))) {
        $return += $this->getChildElementsConfigFormElements($element[$child]);
      }
      else {
        $return += $this->getElementConfigFormElement($child, $element[$child]);
      }
    }

    return $return;
  }

  /**
   * Gets not exportable element types.
   */
  private static function getNotAllowedElementTypes(): array {
    $return = [
      'captcha',
      'container',
      'details',
      'fieldset',
      'password',
      'password_confirm',
      'search',
      'vertical_tabs',
      'view',
      'flexbox',
      'horizontal_rule',
      'section',
    ];

    \Drupal::moduleHandler()->alter('webform_adv_exp_not_allowed_element_types', $return);

    return $return;
  }

}
